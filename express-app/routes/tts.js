const ttw = require("../services/TextToWaveform");

var express = require("express");
var router = express.Router();

/* convert text to speech */
router.get("/waveform", (req, res) => {
  const text = req.query.text || req.body.text || "please stand up again";

  ttw
    .convertTextToWaveform(text)
    .then(result => {
      console.log("result", result);
      res.json(result);
    })
    .catch(err => {
      console.error(err);
      res.send(500).json(err);
    });
});

module.exports = router;
