require("dotenv").config();

const TransloaditClient = require("transloadit");
const path = require("path");
const axios = require("axios");

const transloadit = new TransloaditClient({
  authKey: process.env.TRANSLOADIT_AUTH_KEY,
  authSecret: process.env.TRANSLOADIT_AUTH_SECRET
});

const absPath = path.resolve(__dirname, "../tts_files");
const absFilename = path.join(absPath, "api-tts-convert-demo.mp3");
// console.log("absFilename", absFilename);

transloadit.addFile("myfile_1", absFilename);

const options = {
  params: {
    steps: {
      ":original": {
        robot: "/upload/handle"
      },
      waveformed: {
        use: [":original"],
        robot: "/audio/waveform",
        result: true,
        background_color: "ffffffff",
        center_color: "1974c7aa",
        format: "image",
        height: 410,
        outer_color: "1974c7aa",
        width: 1364
      },
      exported: {
        use: ["waveformed", ":original"],
        robot: "/s3/store",
        credentials: "techstars_s3_credentials"
      }
    }
  }
};

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

transloadit.createAssembly(options, async (err, result) => {
  if (err) {
    throw err;
  }

  //   console.log({ result });
  console.log("result.status_endpoint", result.status_endpoint);

  await sleep(15000);

  if (result.status_endpoint) {
    axios
      .get(result.status_endpoint)
      .then(response => {
        if (
          response.data.results &&
          response.data.results.waveformed &&
          response.data.results.waveformed.length > 0
        ) {
          const img_url = response.data.results.waveformed[0].url;

          if (img_url) {
            console.log("img_url", img_url);
          } else {
            console.log("response.data.results", response.data.results);
          }
        } else if (response.data.results && response.data.results.waveformed) {
          console.log(
            "response.data.results.waveformed",
            response.data.results.waveformed
          );
        } else {
          console.log("response.data", response.data);
        }
      })
      .catch(error => {
        console.log(error);
      });
  }
});
