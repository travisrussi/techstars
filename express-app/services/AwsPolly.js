require("dotenv").config();

const uuidv4 = require("uuid/v4");
const fs = require("fs");

// Load the SDK
const AWS = require("aws-sdk");
const Fs = require("fs");

// Create an Polly client
const Polly = new AWS.Polly({
  signatureVersion: "v4",
  region: "us-west-1"
});

let default_params = {
  OutputFormat: "mp3",
  VoiceId: "Kimberly"
};

async function tts(text, foldername, fileprefix) {
  const params = default_params;
  params.Text = text;

  const guid = uuidv4();
  const filefolder = foldername || "tts_files";
  const filename = `${fileprefix || "speech"}-${guid}.mp3`;
  const filepath = `./${filefolder}/${filename}`;

  fs.existsSync(filefolder) || fs.mkdirSync(filefolder);

  const result = {
    guid,
    params,
    file: {
      filefolder,
      filename,
      filepath
    }
  };

  await new Promise(resolve => {
    Polly.synthesizeSpeech(params, (err, data) => {
      if (err) {
        result.status = "error";
        result.location = "synthesizeSpeech";
        result.error = err;

        console.error("result", result);
        // return result;
      } else if (data) {
        result.data = {
          contentType: data.ContentType,
          requestCharacters: data.RequestCharacters
        };

        if (data.AudioStream instanceof Buffer) {
          Fs.writeFileSync(filepath, data.AudioStream, function(err) {
            if (err) {
              result.status = "error";
              result.location = "synthesizeSpeech.writeFile";
              result.error = err;

              console.error("result", result);
              // return result;
            }
            // console.log("The file was saved!", filepath);
          });
        } else {
          result.error = "data.audiostream not a Buffer";
          console.error("result", result);
        }
      } else {
        result.error = "No data returned from AWS.polly";
        console.error("result", result);
      }

      resolve();
    });
  });
  result.status = result.status || "success";
  return result;
}

module.exports = { tts };
