const awsPolly = require("./AwsPolly");
const transloadIt = require("./TransloadIt");

const text = "please stand up again";

const filefoldername = "tts_files";
const filefolder = `srv/${filefoldername}`;
const fileprefix = "demo-texttowaveform";

awsPolly
  .tts(text, filefolder, fileprefix)
  .then(awsPolly_tts_result => {
    console.log("awsPolly_tts_result", awsPolly_tts_result);

    const waveform_filefolder = `../${filefoldername}`;
    const waveform_filename = awsPolly_tts_result.file.filename;

    transloadIt
      .createWaveform(waveform_filefolder, waveform_filename)
      .then(createWaveform_result => {
        console.log("createWaveform_result", createWaveform_result);
        console.log("waveform_image_url", createWaveform_result.image_url);
      })
      .catch(err => {
        console.error(err);
      });
  })
  .catch(err => {
    console.error(err);
  });
