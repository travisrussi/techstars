require("dotenv").config();

const TransloaditClient = require("transloadit");
const path = require("path");
const axios = require("axios");

const transloadit = new TransloaditClient({
  authKey: process.env.TRANSLOADIT_AUTH_KEY,
  authSecret: process.env.TRANSLOADIT_AUTH_SECRET
});

async function submitWaveform(filename, filepath) {
  const result = {
    step: "createWaveform",
    file: {
      filename,
      filepath
    }
  };

  const absPath = path.resolve(__dirname, filepath);
  const absFilename = path.join(absPath, filename);
  //   console.log("absFilename", absFilename);

  transloadit.addFile("myfile_1", absFilename);

  const options = {
    params: {
      steps: {
        ":original": {
          robot: "/upload/handle"
        },
        waveformed: {
          use: [":original"],
          robot: "/audio/waveform",
          result: true,
          background_color: "ffffffff",
          center_color: "1974c7aa",
          format: "image",
          height: 410,
          outer_color: "1974c7aa",
          width: 1364
        },
        exported: {
          use: ["waveformed", ":original"],
          robot: "/s3/store",
          credentials: "techstars_s3_credentials"
        }
      }
    }
  };

  await new Promise(resolve => {
    transloadit.createAssembly(options, (err, transloadit_result) => {
      if (err) {
        result.error = err;
        result.transloadit_result = transloadit_result;
        resolve();
        //   return result;
      }

      //   console.log({ transloadit_result });
      //   console.log(
      //     "transloadit_result.status_endpoint",
      //     transloadit_result.status_endpoint
      //   );

      result.status_endpoint = transloadit_result.status_endpoint;

      resolve();
    });
  });

  return result;
}

async function checkStatus(status_endpoint) {
  const result = {
    step: "checkStatus",
    status_endpoint
  };

  if (status_endpoint) {
    await new Promise(resolve => {
      axios
        .get(status_endpoint)
        .then(response => {
          result.status_result = response.data.results;

          if (
            response.data.results &&
            response.data.results.waveformed &&
            response.data.results.waveformed.length > 0
          ) {
            const img_url = response.data.results.waveformed[0].url;

            if (img_url) {
              result.image_url = img_url;
              // console.log("img_url", img_url);
            } else {
              console.log("response.data.results", response.data.results);
            }
          } else if (
            response.data.results &&
            response.data.results.waveformed
          ) {
            //   console.log(
            //     "response.data.results.waveformed",
            //     response.data.results.waveformed
            //   );
          } else {
            console.log("response.data", response.data);
          }

          resolve();
        })
        .catch(error => {
          result.error = error;
          console.error(error);
          resolve();
        });
    });

    return result;
  }

  result.error = "no status_endpoint";
  return result;
}

async function createWaveform(filefolder, filename, wait = 15000) {
  const submitWaveform_result = await submitWaveform(filename, filefolder);
  //   console.log("submitWaveform_result", submitWaveform_result);

  await sleep(wait);

  const checkStatus_result = await checkStatus(
    submitWaveform_result.status_endpoint
  );

  //   console.log("checkStatus_result", checkStatus_result);

  return {
    request: submitWaveform_result,
    status: checkStatus_result,
    image_url: checkStatus_result.image_url
  };
}

function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}

module.exports = { submitWaveform, checkStatus, createWaveform };
