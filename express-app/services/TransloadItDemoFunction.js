const transloadIt = require("./TransloadIt");

const demo_filefolder = "../tts_files";
const demo_filename = "api-tts-convert-demo.mp3";

transloadIt
  .createWaveform(demo_filefolder, demo_filename)
  .then(createWaveform_result => {
    console.log("createWaveform_result", createWaveform_result);
    console.log("waveform_image_url", createWaveform_result.image_url);
  })
  .catch(err => {
    console.error(err);
  });

//   createWaveform_result { request:
//     { step: 'createWaveform',
//       file:
//        { filename: 'api-tts-convert-demo.mp3',
//          filepath: '../tts_files' },
//       status_endpoint:
//        'https://api2-naju.transloadit.com/assemblies/b5caef20362b11e9943eb13d2c9e2d78' },
//    status:
//     { step: 'checkStatus',
//       status_endpoint:
//        'https://api2-naju.transloadit.com/assemblies/b5caef20362b11e9943eb13d2c9e2d78',
//       status_result: { waveformed: [Array] },
//       image_url:
//        'http://tmp.transloadit.com.s3.amazonaws.com/b9260100362b11e986056100fb424f23.png' },
//    image_url:
//     'http://tmp.transloadit.com.s3.amazonaws.com/b9260100362b11e986056100fb424f23.png' }
//  waveform_image_url http://tmp.transloadit.com.s3.amazonaws.com/b9260100362b11e986056100fb424f23.png
