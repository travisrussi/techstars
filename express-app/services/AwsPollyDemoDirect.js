require("dotenv").config();

const uuidv4 = require("uuid/v4");

// Load the SDK
const AWS = require("aws-sdk");
const Fs = require("fs");

// Create an Polly client
const Polly = new AWS.Polly({
  signatureVersion: "v4",
  region: "us-west-1"
});

let params = {
  Text: "Hi, my name is crazy monkey.",
  OutputFormat: "mp3",
  VoiceId: "Kimberly"
};

Polly.synthesizeSpeech(params, (err, data) => {
  if (err) {
    console.log(err.code);
  } else if (data) {
    if (data.AudioStream instanceof Buffer) {
      const guid = uuidv4();
      Fs.writeFile(`./speech-${guid}.mp3`, data.AudioStream, function(err) {
        if (err) {
          return console.log(err);
        }
        console.log("The file was saved!");
        console.log(`speech-${guid}.mp3`);
      });
    }
  }
});
