const awsPolly = require("./AwsPolly");
const transloadIt = require("./TransloadIt");

const filefoldername = "tts_files";
const filefolder = `${filefoldername}`;
const fileprefix = "api-texttowaveform";

async function convertTextToWaveform(textToConvert, output_console = false) {
  const result = {};

  await awsPolly
    .tts(textToConvert, filefolder, fileprefix)
    .then(async awsPolly_tts_result => {
      if (output_console)
        console.log("awsPolly_tts_result", awsPolly_tts_result);

      result.tts_result = awsPolly_tts_result;
      result.image_text = awsPolly_tts_result.params.Text;

      const waveform_filefolder = `../${filefoldername}`;
      const waveform_filename = awsPolly_tts_result.file.filename;

      await transloadIt
        .createWaveform(waveform_filefolder, waveform_filename)
        .then(createWaveform_result => {
          if (output_console) {
            console.log("createWaveform_result", createWaveform_result);
            console.log("waveform_image_url", createWaveform_result.image_url);
          }

          result.waveform_result = createWaveform_result;
          result.image_url = createWaveform_result.image_url;
          return result;
        })
        .catch(err => {
          console.error(err);
        });
    })
    .catch(err => {
      console.error(err);
    });

  return result;
}

module.exports = { convertTextToWaveform };
