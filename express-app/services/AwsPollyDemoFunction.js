// import { tts } from "AwsPolly";
const awsPolly = require("./AwsPolly");

const text = "please stand up again";
const filefolder = "srv/tts_files";
const fileprefix = "demo-function";
const result = awsPolly.tts(text, filefolder, fileprefix);

console.log("result", result);

// result { guid: '3c609371-bc05-447a-acd4-199f58dc1c60',
//   params:
//    { OutputFormat: 'mp3',
//      VoiceId: 'Kimberly',
//      Text: 'please stand up again' },
//   file:
//    { filefolder: 'srv/tts_files',
//      filename: 'demo-function-3c609371-bc05-447a-acd4-199f58dc1c60.mp3',
//      filepath:
//       './srv/tts_files/demo-function-3c609371-bc05-447a-acd4-199f58dc1c60.mp3' },
//   status: 'success' }
