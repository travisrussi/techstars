# Overview

Simple web app that takes a text string, converts it to speech (text-to-speech), converts the MP3 speech audio file into a waveform image, and displays the waveform on the page.

[Demo on Heroku](https://techstars-travis-vuejs.herokuapp.com)
[Express Backend Demo on Heroku](https://techstars-travis-express.herokuapp.com)

## Third-Party Dependencies

For the Text-To-Speech, we are using [AWS Polly](https://aws.amazon.com/polly/).

For the Waveform image generation, we are using [TransloadIt](https://transloadit.com).

For logging and app usage statistics, we are using [New Relic](https://newrelic.com).

## Environment Variables (Configuration)

In the Express app, the audio files from AWS Polly are written into a local `tts-files` folder.

The `express-app` depends on a couple ENV variables:

```bash
AWS_ACCESS_KEY_ID = 123
AWS_SECRET_ACCESS_KEY = 456

TRANSLOADIT_AUTH_KEY = abc
TRANSLOADIT_AUTH_SECRET = dev

NEWRELIC_APPNAME = SomeName
NEWRELIC_KEY = SomeKey
NEWRELIC_LOGLEVEL = SomeLevel
```

The `vue-app` API service (`vue-app\services\Api.js`) has the axios API `baseurl` hard-coded to the Heroku instance hosting the `express-app`.

```js
export default () => {
  return axios.create({
    baseURL: "https://techstars-travis-express.herokuapp.com",
    withCredentials: false,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    }
  });
};
```

## Run VUE app for development (using nodemon)

```bash
cd vue-app
npm run build # if you want to use npm run serve to mimic productio
npm run dev
```

## Run Express app for development

```bash
cd express-app
npm run dev
```

## Deploy to Heroku

We push each app seperately.

### For Express

```bash
git subtree push --prefix express-app/ heroku-express master
```

### For VUE

```bash
git subtree push --prefix vue-app/ heroku-vue master
```

### Tutorial used for setup

Initial build from scratch of VUE and EXPRESS
https://medium.com/@Dongmin_Jang/nodejs-express-vue-heroku-d3ed48edee0c

To only deploy a subfolder to Heroku
https://coderwall.com/p/ssxp5q/heroku-deployment-without-the-app-being-at-the-repo-root-in-a-subfolder
