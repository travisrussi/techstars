import Api from "@/services/Api";

export default {
  convertTextToSpeech(params) {
    return Api().get("/tts/convert?text=" + params.text);
  },
  convertTextToWaveform(params) {
    return Api().get("/tts/waveform?text=" + params.text);
  }
};
