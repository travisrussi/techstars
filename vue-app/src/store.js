import Vue from "vue";
import Vuex from "vuex";

import TtsService from "@/services/TtsService";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    status: "completed",
    tts_request: {},
    tts_response: {}
  },
  getters: {
    status: state => {
      return state.status;
    },
    tts: state => {
      return {
        request: state.tts_request,
        response: state.tts_response
      };
    },
    tts_request: state => {
      return state.tts_request;
    },
    tts_response: state => {
      return state.tts_response;
    }
  },
  mutations: {
    setStatus(state, status) {
      state.status = status;
    },
    createTtsRequest(state, tts_request) {
      state.tts_request = tts_request;
    },
    setTtsResponse(state, tts_response) {
      state.tts_response = tts_response;
    }
  },
  actions: {
    SET_STATUS: async function({ commit }, status) {
      console.log("SET_STATUS, status = ", status);
      commit("setStatus", status);
    },
    CONVERT_TEXT_TO_SPEECH: async function({ commit }, text) {
      commit("setStatus", "pending");
      console.log("CONVERT_TEXT_TO_SPEECH, text = ", text);
      const response = await TtsService.convertTextToSpeech({ text });
      const ttsResponse = response.data;
      console.log("ttsResponse: ", ttsResponse);

      commit("setTtsResponse", ttsResponse);
      if (
        ttsResponse == null ||
        ttsResponse.status != "success" ||
        ttsResponse.err != null
      ) {
        console.error("Your text could not be converted to speech.");
        commit("setStatus", "error");
        return false;
      }

      commit("setStatus", "complete");
      return true;
    },
    CONVERT_TEXT_TO_WAVEFORM: async function({ commit }, text) {
      commit("setStatus", "pending");
      console.log("CONVERT_TEXT_TO_WAVEFORM, text = ", text);
      const response = await TtsService.convertTextToWaveform({ text });
      const ttsResponse = response.data;
      console.log("ttsResponse: ", ttsResponse);

      commit("setTtsResponse", ttsResponse);
      if (ttsResponse == null || ttsResponse.image_url == null) {
        console.error("Your text could not be converted to waveform.");
        commit("setStatus", "error");
        return false;
      }

      commit("setStatus", "complete");
      return true;
    }
  }
});
